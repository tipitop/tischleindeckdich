package de.tip.tischleindeckdich.domain

data class User(val name: String)