package de.tip.tischleindeckdich.domain

data class Place(val name: String, val adress: Adress)
data class Adress(val street: String, val coordinates: Pair<Double, Double>)
