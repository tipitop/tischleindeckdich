package de.tip.tischleindeckdich

import io.micronaut.runtime.Micronaut

object Application {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
                .packages("de.tip.tischleindeckdich")
                .mainClass(Application.javaClass)
                .start()
    }
}